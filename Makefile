CC=g++
CFLAGS=-I.
DEPS = assert.h crypto-layout.h tests.h

%.o: %.c $(DEPS)
		$(CC) -c -o $@ $< $(CFLAGS)

pkcryptomake: main.o tests.o primenumber.o message.o rsakeypair.o
	g++ -o pkcryptomake main.o tests.o rsakeypair.o message.o primenumber.o -I.
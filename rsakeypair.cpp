#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "assert.h"
#include "crypto-layout.h"
#include "tests.h"


/*******************************************************************************************
 *
 *		RSAKey
 *
 *		this object encapsulates a single RSA asymmetric key - either public or private. Also
 *		contains some useful methods for manipulating a key.
 *
 ********************************************************************************************/

// prints this key (the exponent and prime number) in a standard format, base64 encoded and
// with a fixed width
//
//  @retvalue: *buffer for ease of use
char*	RSAKey::prettyPrint( char** buffer )
{
	/*
	 * FORMAT IS AS FOLLOWS
	*		 0										39									   79			= 80 total columns fixed width
	 *		 ^                                       ^                                     ^
	 * 		"+++++++++++++++++++++++++++++++++ START RSA KEY ++++++++++++++++++++++++++++++++\n"
	 *		<base64 encoded string containing the following data:
	 *				(4 bytes : Build Version)
	 *				(8 bytes : Exponent)
	 *				(8 Bytes : Prime Component)
	 *		"+++++++++++++++++++++++++++++++++  END  RSA KEY ++++++++++++++++++++++++++++++++\n"
	 */

	char out[2048], *tmp;
	u8 scratch[128];

	u32 used = 0, outLen = 0;

	// copy over the header

	memcpy( &out[0], "+++++++++++++++++++++++++++++++++ START RSA KEY ++++++++++++++++++++++++++++++++\n\0", strlen("+++++++++++++++++++++++++++++++++ START RSA KEY ++++++++++++++++++++++++++++++++\n")+1 );
	outLen = strlen(&out[0]);

	// build our data structure

	*((u32*)&scratch[used]) = BuildVersion;
	used+=sizeof(u32);

	*((u64*)&scratch[used]) = Exponent;
	used+=sizeof(Exponent);

	*((u64*)&scratch[used]) = Prime.getValue();
	used+=sizeof(Prime.getValue());
/*
	printf( "HEX VIEW: " );

	for( int b = 0; b < used; b ++ )
		printf( "%X ", scratch[(int)b] );
	printf( "\n" );*/

	// convert it to base 64

	base64Enc((u8*)&scratch[0],used,&tmp);

	// now we need to loop through and break it into 80 char wide lines

	u32 b64len = strlen(tmp);
	u32 extra = b64len%80;
	u32 fullCount = (b64len-extra)/80;

	for(int i = 0; i < fullCount; i++ )
	{
		memcpy( &out[outLen], &tmp[i*80], 80 );
		outLen += 80;
		out[outLen] = '\n';
		outLen++;
	}

	if( extra )
	{
		memcpy( &out[outLen], &tmp[fullCount * 80], extra );
		outLen += extra;
		out[outLen] = '?';
		outLen ++;
		out[outLen] = '~';
		outLen ++;
		out[outLen] = '\n';
		outLen ++;
	}

	// copy over our footer

	memcpy( &out[outLen], "+++++++++++++++++++++++++++++++++  END  RSA KEY ++++++++++++++++++++++++++++++++\n\0", strlen("+++++++++++++++++++++++++++++++++  END  RSA KEY ++++++++++++++++++++++++++++++++\n")+1);
	outLen = strlen(out)+1;


	*buffer = new char[outLen];

	memcpy(*buffer, out, outLen);

	return *buffer;
}

// loads this object from a prettyPrinted RSAKey null-terminated string (as printed by RSAKey::prettyPrint())
//
//  @retvalue: true on success, false on failure
bool RSAKey::loadFromString( char* data )
{
	if( !data )
		return false;

	u32 length = strlen(data);

	if( length < 163 )
	{
		printf("RSAKEY STRING IS TOO SHORT\n");
		// our header and footer with 0 data still occupies 162 bytes of memory
		return false;
	}

	// check this buffer for validity and grab data as we go

	char* ptr = data;

	if( memcmp(ptr,"+++++++++++++++++++++++++++++++++ START RSA KEY ++++++++++++++++++++++++++++++++\n",strlen("+++++++++++++++++++++++++++++++++ START RSA KEY ++++++++++++++++++++++++++++++++\n")) != 0 )
	{
		printf( "INVALID RSAKEY HEADER\n");
		return false;
	}

	u32 ptrIndex = strlen("+++++++++++++++++++++++++++++++++ START RSA KEY ++++++++++++++++++++++++++++++++\n");

	u32 Version;

	u32 lineLength = 0;

	char bigBuffer[2048];
	u32	 bigIndex = 0;

	char line[128];
	u8	 index = 0;
	bool endOfData = false;

	while( ptrIndex < length )
	{
		if( ptr[ptrIndex] == '?' )			// end of data section
		{
			if( ptr[ptrIndex+1] != '~' )
			{
				// corrupted	
				printf("RSAKEY:LOAD CORRUPTED END OF DATA SECTION MARKER %c\n", ptr[ptrIndex+1]);

				return false;
			}

			ptrIndex++;
			ptrIndex++;

			endOfData = true;
		}
		else if( ptr[ptrIndex] != '\n' )
		{
			line[index] = ptr[ptrIndex];
			index++;
			ptrIndex++;
		}
		else
		{
			memcpy(&bigBuffer[bigIndex], &line[0], index);

			bigIndex += index;

			line[index] = '\0';

			index = 0;
			ptrIndex++;

			if( endOfData )
				break;
		}
	}

	if( memcmp(&ptr[ptrIndex], "+++++++++++++++++++++++++++++++++  END  RSA KEY ++++++++++++++++++++++++++++++++\n",strlen("+++++++++++++++++++++++++++++++++  END  RSA KEY ++++++++++++++++++++++++++++++++\n")) != 0 )
	{
		printf("RSAKEY FOOTER CORRUPTED.\n");
	}

	bigBuffer[bigIndex] = '\0';

	u8* DecodedData;
	u32 dataLen;

	//printf( "%s\n", bigBuffer );

	base64Dec(bigBuffer, bigIndex, &DecodedData, &dataLen );
/*
	printf( "HEX VIEW: " );

	for( int b = 0; b < dataLen; b ++ )
		printf( "%X ", DecodedData[(int)b] );

	printf( "\n" );*/

	Version = *((u32*)&DecodedData[0]);
	Exponent = *((u64*)&DecodedData[4]);
	Prime.setValue( *((u64*)&DecodedData[12]) );

	return true;
}

/*******************************************************************************************
 * 
 * 		RSAKeyPair
 * 
 * 		this object encapsulates an entire public and private key pair
 * 			- generating a new key
 * 			- destroying / invalidating an old one
 *			- exporting key in base64 format
 * 			- importing a key given in base64 format
 * 			- formatting, seeding, and other administrative work with a key pair
 * 
 * *****************************************************************************************/

RSAKeyPair::RSAKeyPair()
{
}

RSAKeyPair::~RSAKeyPair()
{
}

// generates a new key pair 
// the return value is Precision if success, or a series of negative error messages.

bool RSAKeyPair::generateKey()
{
	PrimeNumber P, Q;
	u64 PublicK, PrivateK;

	srand(time(NULL));

	u64 value = (rand()/((float)RAND_MAX))*PRIME_RANGE+PRIME_MIN;
	P.generatePrime(value);
	value = (rand()/((float)RAND_MAX))*PRIME_RANGE+PRIME_MIN;
	Q.generatePrime(value);

	// we need a special property of relatively prime numbers to make this work
	// so we need the gcd of the exponent, and a special value calleD PHI to be 1, or relatively prime.
	// keep trying random values until we find one

	u64 phi = (P.getValue()-1) * (Q.getValue()-1);

	do
	{
		PublicK = round( ((double)rand()/((float)RAND_MAX))*K_RANGE+K_MIN );

	} while(PrimeNumber::gcd(PublicK, phi) != 1);

	s64 vals[3], a,b;

	a = PublicK;
	b = -1 * P.getValue() * Q.getValue();

	PrimeNumber::xeuclid( a, b, vals );  

	PrivateK = vals[0];

	// set our internal variables

	Public.Exponent = PublicK;
	Public.Prime    = Q;

	Private.Exponent = PrivateK;
	Private.Prime 	 = P;


	return true;
}
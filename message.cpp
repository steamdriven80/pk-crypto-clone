#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "assert.h"
#include "crypto-layout.h"
#include "tests.h"

/***********************************************************************
 * base64Enc() and base64Dec()
 *
 *	These functions are vital for working with these huge numbers.  Printing
 *  them to screen, reviewing them, saving/loading them in ASCII format - all
 *  immeasurably easier using base64
 ************************************************************************/

char base64Lookup[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* 	base64Enc( u8* input, u32 inputLen, char** outputBuffer )
{
	// check parameters
	if( !input || !inputLen || !outputBuffer )
	{
		*outputBuffer = NULL;
		return NULL;
	}

	u32 extra 		= inputLen % 3;
	u32 fullCount 	= (inputLen - extra) / 3;

	// allocate the output buffer 
	// (4 bytes for every 3 bytes of input plus padding bytes and a termination value)
	*outputBuffer = new char[(fullCount + 2)*4 + 1];

	char* bufferPtr = *outputBuffer;

	ASSERT(bufferPtr != NULL && *outputBuffer != NULL);


	u32 outValues[4], inValues[3];

	// process all the full 3-byte groups of data
	for( u32 i = 0; i < fullCount; i ++ )
	{
		// grab the 4 base64 values from the 3 input bytes (we couldn't condensed this significantly, but at the
		// expense of ledgibility. Therefore, here is highly readable, but marginally inefficient code)

		inValues[0] = input[i*3];
		inValues[1] = input[i*3+1];
		inValues[2] = input[i*3+2];

		outValues[0] = inValues[0] & (0x3F);
		outValues[1] = ( ( (inValues[1] << 8) | (inValues[0]) ) & (0xFC0) ) >> 6;
		outValues[2] = ( ( (inValues[2] << 16) | (inValues[1] << 8) | (inValues[0]) ) & (0x3F000) ) >> 12;
		outValues[3] = ( ( (inValues[2] << 16) | (inValues[1] << 8) | (inValues[0]) ) & (0xFC0000) ) >> 18;

		for( int j = 0; j < 4; j ++ )
		{
			// make DOUBLY sure all values are in range (should be, we used bit operations)

			ASSERT( outValues[j] < 64 );
			outValues[j] &= 0x3F;

			*(bufferPtr) = base64Lookup[outValues[j]];

			bufferPtr ++;
		}

	}

	// now we process any extra bytes

	switch( extra )
	{
	case 1:					// one extra byte, we pad the remaining space with zero bits (i.e. we create fake base64 values)
							// and then we append '=' to the end of the buffer to indicate the padding

		// grab the 4 base64 values from the 3 input bytes (we couldn't condensed this significantly, but at the
		// expense of ledgibility. Therefore, here is highly readable, but marginally inefficient code)

		inValues[0] = input[(fullCount*3)];
		inValues[1] = 0;					// so easy! because we wrote this code out instead of using shorthand
		inValues[2] = 0;

		outValues[0] = inValues[0] & (0x3F);
		outValues[1] = ( ( (inValues[1] << 8) | (inValues[0]) ) & (0xFC0) ) >> 6;
		outValues[2] = ( ( (inValues[2] << 16) | (inValues[1] << 8) | (inValues[0]) ) & (0x3F000) ) >> 12;
		outValues[3] = ( ( (inValues[2] << 16) | (inValues[1] << 8) | (inValues[0]) ) & (0xFC0000) ) >> 18;

		for( int j = 0; j < 4; j ++ )
		{
			// make DOUBLY sure all values are in range (should be, we used bit operations)

			ASSERT( outValues[j] < 64 );
			outValues[j] &= 0x3F;

			*(bufferPtr) = base64Lookup[outValues[j]];

			bufferPtr ++;
		}

		*bufferPtr = '=';
		bufferPtr ++;

		break;

	case 2:					// two extra bytes, we pad the remaining space with zero bits (i.e. we create fake base64 values)
							// and then we append '==' to the end of the buffer to indicate the padding

		// grab the 4 base64 values from the 3 input bytes (we couldn't condensed this significantly, but at the
		// expense of ledgibility. Therefore, here is highly readable, but marginally inefficient code)

		inValues[0] = input[fullCount*3];
		inValues[1] = input[fullCount*3+1];
		inValues[2] = 0;

		outValues[0] = inValues[0] & (0x3F);
		outValues[1] = ( ( (inValues[1] << 8) | (inValues[0]) ) & (0xFC0) ) >> 6;
		outValues[2] = ( ( (inValues[2] << 16) | (inValues[1] << 8) | (inValues[0]) ) & (0x3F000) ) >> 12;
		outValues[3] = ( ( (inValues[2] << 16) | (inValues[1] << 8) | (inValues[0]) ) & (0xFC0000) ) >> 18;

		for( int j = 0; j < 4; j ++ )
		{
			// make DOUBLY sure all values are in range (should be, we used bit operations)

			ASSERT( outValues[j] < 64 );
			outValues[j] &= 0x3F;

			*(bufferPtr) = base64Lookup[outValues[j]];

			bufferPtr ++;
		}

		*bufferPtr = '=';
		bufferPtr ++;

		*bufferPtr = '=';
		bufferPtr ++;

		break;
	}


	// append the null termination byte

	*bufferPtr = '\0';

	return *outputBuffer;
}

/*****************************************************************************************************/

int		base64Reverse( char encoded )
{
	if( encoded >= 'A' && encoded <= 'Z' )
		return encoded-'A';

	if( encoded >= 'a' && encoded <= 'z' )
		return encoded-'a'+26;

	if( encoded >= '0' && encoded <= '9' )
		return encoded-'0'+52;

	if( encoded == '+' )
		return 62;

	if( encoded == '/' )
		return 63;

	// bad base 64 value
	// allow to proceed with a null value, and print a warning

	printf( "[\033[22;33mWarning\033[0m] : %s:%d -> Found Invalid base64 Reverse value '%d'\n", __FILE__, __LINE__, encoded );

	return 0;
}

u8* 	base64Dec( char* input, u32 inputLen, u8** outputBuffer, u32* outputLen )
{
	// check parameters
	if( !input || !inputLen || !outputBuffer || !outputLen )
	{
		*outputBuffer = NULL;
		*outputLen	  = 0;

		return NULL;
	}


	// clear the parameters, to be safe

	*outputBuffer = NULL;
	*outputLen	  = 0;


	// our output can't be bigger than our input (bas64 is less space efficient than base256 when used with 8-bit
	// numbers
	u8* scratchPad  = new u8[inputLen];
	u8* scratch = scratchPad;				// incrementable pointer

	u32 inValues[4];

	u32 extra     = inputLen % 4;
	u32 fullCount = (inputLen - extra) / 4;

	for( u32 i = 0; i < fullCount; i++ )
	{
		// this converts the ASCII representation of base64 into actual numbers

		inValues[0] = base64Reverse(input[i*4]);
		inValues[1] = base64Reverse(input[i*4+1]);
		inValues[2] = base64Reverse(input[i*4+2]);
		inValues[3] = base64Reverse(input[i*4+3]);

		// this builds 3 output bytes from 4 input base64 bytes

		scratch[0] = (u8) (((inValues[1] << 6) | (inValues[0])) & 0xFF);
		scratch[1] = (u8) (((inValues[2] << 4) | (inValues[1] >> 2)) & 0xFF);
		scratch[2] = (u8) (((inValues[3] << 2) | (inValues[2] >> 4)) & 0xFF);

		scratch++;
		scratch++;
		scratch++;
	}

	// now we check for padded bytes

	switch( extra )
	{
	case 1:			// original data had 1 byte extra, so they padded 2 bytes, and added '='

		ASSERT( input[fullCount*4] == '=' );

		// now we need to chop the last 2 bytes off our output buffer

		scratch--;
		scratch--;

		break;

	case 2:		// original data has 2 bytes extra, so they padded 1 byte, and added '=='

		ASSERT( input[fullCount*4] == '=' && input[fullCount*4 + 1] == '=' );

		// now we need to chop the last byte off our output buffer

		scratch--;

		break;

	case 3:

		// should never happen.  CORRUPT DATA.

		printf( "[\033[22;33mWarning\033[0m] : %s:%d -> base64Dec Data Corrupted.  Invalid data length. Final Value: %dd/%cc\n", __FILE__, __LINE__, input[inputLen-1],input[inputLen-1] );

		delete scratchPad;

		*outputBuffer = NULL;
		*outputLen	  = 0;

		return NULL;
	}

	// find the length of the decoded data
	*outputLen = (u32) (scratch - scratchPad);

	if( *outputLen > inputLen )
	{
		printf( "[\033[22;31m Error \033[0m] : %s:%d -> base64Dec Memory Fault Detected!!  Scratchpad Buffer Length has exceeded Input Buffer Length. How?\n", __FILE__, __LINE__ );

		return NULL;
	}

	if( !*outputLen )
	{
		printf( "[\033[22;33mWarning\033[0m] : %s:%d -> base64Dec Scratchpad did not yield any usable data.  Unknown cause.\n", __FILE__, __LINE__ );

		return NULL;
	}

	// allocate and copy over from our scratchpad to our output buffer
	*outputBuffer = new u8[*outputLen];

	ASSERT( *outputBuffer != NULL );

	memcpy( *outputBuffer, scratchPad, *outputLen );


	// free our scratchpad

	delete scratchPad;




	return *outputBuffer;
}

/*******************************************************************************************
 * 
 * 		Message
 * 
 * 		this object encapsulates a entire message.  The typical order of function calls would
 *		be as follows:
 *			1.  Grab Message From User 			(stdin?)
 *			2. 	Encode the message 				encodeMsg()
 *				(this removes arbitrary characters, and compacts the data into a smaller field.)
 *			3.	Encrypt the message 			encryptMsg() 
 *	[optional]	Sign the message to prove authenticity
 *			4. 	Communicate the encrypted message
 *	[optional]  Verify message signature with sender public key
 *			5.  Receiver Decrypts the message 	decryptMsg()
 *			6.	Receiver Decodes the message 	decodeMsg()
 *			
 * 
 * *****************************************************************************************/

Message::Message():
Data(NULL),Length(0)
{
}

Message::~Message()
{
	if( Data )
	{
		delete Data;

		Data = NULL;
	}
}

// Encrypts an encoded message using a recipients public key
//
//	 @retvalue:	length of output buffer

u32 Message::encryptMsg( char* encodedMsg, char** outputBuffer, RSAKey PublicKey )
{
	u32 dataLength = strlen(encodedMsg);

	return 0;
}

// Signs an encrypted message with a private key using the length of the encryptedMsg and a randomized salt value
//
//	 @retvalue: true on success, false if there was an error (most likely the key is invalid)

bool Message::signMsg( u8* encryptedMsg, u32 byteLength, RSAKey SignersPrivateKey )
{
	return false;
}

// Verifies the signature (if any) on an encrypted message using the [expected] senders public key.
// If there is no signature at all, DefaultPass defines the behavior.  If there is a signature but it does
// not match, always return false.
//
// 	 @retvalue: true if signature matches (or NO signature with DefaultPass==true) or false otherwise

bool Message::signVerify( u8* encryptedMsg, u32 byteLength, RSAKey SignersPublicKey, bool DefaultPass )
{
	return false;
}

// Decrypts an encoded message using a private key
//
//	 @retvalue:	length of output buffer

u32 Message::decryptMsg( char* encodedMsg, char** outputBuffer, RSAKey PrivateKey )
{
	return 0;
}

// Prints the contents of the internal buffer using base64 encoding and a fixed width/format (uses newLine for newlines)

char* Message::prettyPrint( char** buffer, char newLine )
{
	return NULL;
}
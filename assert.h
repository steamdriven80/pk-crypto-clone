#ifndef _ASSERT_H_
#define _ASSERT_H_

#include <assert.h>

#define ASSERT(expr) assert(expr)

#endif
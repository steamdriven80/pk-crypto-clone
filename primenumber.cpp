#include <vector>
#include <cmath>

#include <stdlib.h>

#include "assert.h"
#include "crypto-layout.h"
#include "tests.h"

u64 dMod( u64 a, u64 mod )
{
	return (a - (floor(a / mod) * mod));
}

PrimeNumber::PrimeNumber()
{

}
		
PrimeNumber::~PrimeNumber()
{
	
}

// attempts to generate a prime of minDigits precision using brute force
// DON'T try this for large primes use the methods below
		
u64 PrimeNumber::generatePrime( u64 minimum )
{
	if( minimum < 2 )
	{
		Value = 2;
		return Value;
	}

	for (u64 i = minimum; i >= minimum; i++)
	{
		bool isprime = true;

		u64 root = ceil(sqrt(i));

		for (u64 j = 2; j < root; j++)
		{
			if( dMod(i,j) == 0 )
			{
				isprime = false;
				j = root;
			}
		}

		if( isprime == true )
		{
			if( i > minimum )
			{
				Value = i;
				return i;
			}
		}
	}

	return 0;
}

bool PrimeNumber::isMersennePrime( u64 prime )
{
	u64 i = 0;

	while (pow(2, i) < prime)
	{
		i++;
		if ((u64)(pow(2, i) - 1) == prime)
			return true; 
	}

	return false;
}

bool PrimeNumber::checkPrime(u64 prime)
{
	u64 size = ceil(sqrt(prime));

	if( prime < 2 )
		return false;

	for (u64 j = 2; j < size; j++)
		if (dMod(prime,j) == 0) return false;

	return true;
}

// successive square function that raises base ^ power (mod modulo)
u64 PrimeNumber::sSquare( u64 base, u64 exponent, u64 modulo )
{
	///////////////////////////////////
	// binary expansion of the power
	//
	// we're going to discover the largest power of 2 we'll need
	// right now only handles up to 32-bit powers

	u64 maxBinary = 0;
	u32 flag, i=0;

	for(i=0; i<32; i++)
	{
		flag = 1<<i;

		if( (exponent) & flag )
		{
			maxBinary = i;
		}
	}

	std::vector<u64> powerList;

	u64 value = base;

	powerList.push_back( value ); // push base ^ 1

	// build the successive squaring list for powers of 2 up to our max

	for(i=1; i<= maxBinary; i++) 
	{
		// square the values (mod modulo)
		value = dMod((value * value), modulo);

		powerList.push_back(value);
	}

	// iterate through our binary expansion (we're cheating and using the fact that #'s are stored in binary form)
	// and multiply together the powers of two, mod modulo each time
	value = 1;

	for(i=0; i<32; i++) 
	{
		flag = 1 << i;

		if( exponent & flag )
		{
			value = dMod((value * powerList[i]), modulo);
		}
	}

	// value is the final successively squared result
	return value;
}

// finds the gcd
u64 PrimeNumber::gcd(u64 a, u64 b)
{
	if (b == 0)
	{
		return a;
	}

	return gcd(b, dMod(a,b));
}

// extended euclidean algorithm (recursive)
// result is a 3 element int array
void PrimeNumber::xeuclid(s64 a, s64 b, s64 *result )
{
	if (b == 0)
	{
		result[0] = 1;
		result[1] = 0;
		result[2] = a;
	}
	else
	{
		// elements in euclids algorithm
		// x and y are factors, divisor is the whole number divisor of the previous result

		s64 recResult[3], x, y, divisor;

		xeuclid(b, dMod(a,b), recResult);

		x 		= recResult[0];
		y 		= recResult[1];
		divisor = recResult[2];

		result[0] = y;
		result[1] = x-y*floor(a/b);
		result[2] = divisor;
	}
}

// finds the kth root of the equation X ^ exponent = power (mod pq)
u64 PrimeNumber::kRoot( u64 exponent, u64 power, u64 p, u64 q ) 
{
	if( gcd( p, q ) != 1 )		// must have no common factors (relatively prime)
		return 0;

	printf( "x ^ %lld = %lld (mod %lld)\n", exponent, power, p*q);

	// this function does not test if p and q are absolutely prime, it will give wonky values if they're not
	// use a negative phi value for extended euclidean
	s64 phi = (p-1) * (q-1);
	s64 solution[3];

	printf( "phi: %lld\n", phi );

	xeuclid( exponent, phi, solution );

	// if equation solution are not positive numbers, make them so
	if( solution[0] < 0 || solution[1] < 0 )
	{
		solution[0] += phi;
		solution[1] += exponent;
	}

	return sSquare(power, solution[0], (p*q));
}

u32 prand(u32 min, u32 max)
{
	
	return (((float) rand() / (float) RAND_MAX) * (max-min)) + min;
}/*

void PrimeNumber::generateKey( u64* public, u64* private, u64* exp )
{
	PrimeNumber p, q, k;

	p.generatePrime(prand(1000,10000));
	q.generatePrime(prand(1000,10000))
	k.generatePrime(prand(20,50));

	*public = p.Value;

	*private = q.Value;

	*exp 	 = k.Value;
}*/
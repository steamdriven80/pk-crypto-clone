#ifndef _TESTS_H_
#define _TESTS_H_

#include <stdio.h>

#define TestGroup_Invalid 0
#define TestGroup_nint 1
#define TestGroup_PrimeNumber 2
#define TestGroup_Base64 3
#define TestGroup_Message
#define TestGroup_UI

#define BlankStatusArgs(caption,args) printf( TestStringy([--------] caption\r), args )
#define PassStatusArgs(caption,args)  printf( TestStringy([---\033[22;32mOK\033[0m---] caption\n), args )
#define FailStatusArgs(caption,args)  printf( TestStringy([-\033[22;31mFAILED\033[0m-] caption\n), args )
#define InfoStatusArgs(caption,args)  printf( TestStringy([--\033[22;34minfo\033[0m--] caption\n), args )

#define BlankStatus(caption) printf( TestStringy([--------] caption\r) )
#define PassStatus(caption)  printf( TestStringy([---\033[22;32mOK\033[0m---] caption\n) )
#define FailStatus(caption)  printf( TestStringy([-\033[22;31mFAILED\033[0m-] caption\n) )
#define InfoStatus(caption)  printf( TestStringy([--\033[22;34minfo\033[0m--] caption\n) )

#define TestStart(name,enable) printf( TestStringy(\n\033[22;36m++++++++++ GROUP START :\033[0m name\n) ); if( enable )
#define TestAssert(expr) BlankStatus(expr); if( !(expr) ) FailStatus(expr); else PassStatus(expr)

#define TERM_RESETCOLOR \033[0m
#define TEST_PASSCOLOR \033[22;32m
#define TEST_WARNCOLOR \033[22;33m
#define TEST_FAILCOLOR \033[22;31m
#define TEST_INFOCOLOR \033[22;34m
#define TEST_HEADERCOLOR \033[22;36m

#define TestStringy(expr) #expr
/*
 *	testGroup
 *
 *		groupName - a string indicating the group to test
 *
 *	@bool : true for success, false for failure of at least one test in group
 *
 *		our public 'interface' to our testing framework, that performs automated, repeatable tests
 *		of all our code.  The asserts() dispersed throughout the source code will halt execution during
 *		debugging, but will be preprocessor replaced with a logging version during production.
 */ 

bool testGroup( u32 groupName = TestGroup_Invalid );		// a null value will return success.



class Tests
{
	protected:

		friend bool testGroup( u32 groupName );
		static bool testNINT();
		static bool testPRIME();
		static bool testBASE64();
};


#endif
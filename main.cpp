#include <stdio.h>
#include "assert.h"

#include "crypto-layout.h"

#include "tests.h"

int main( void )
{
	printf( "RSA Public Key Encryption Group Project v0.78\n" );
	printf( "COP 3503 - Team 1\n" );
	printf( "Programmer: Chris Pergrossi\n\n" );
	
	printf( "Running Diagnostic Self-Tests.\n" );

	///////////////////////////////////////////////////////////////////////////////////
	//
	// in order to use the incomplete library, we demonstrate indiviual function effect
	// and correctness of execution using our Testing Framework

	testGroup( TestGroup_PrimeNumber );
	testGroup( TestGroup_Base64 );

	///////////////////////////////////////////////////////////////////////////////////

	printf("Tests Complete.\n\n");
	printf("More Info: File Listing\n");
        printf("\tmain.cpp - contains the program entry point\n");
	printf("\tnint.cpp - contains a n-precision large int class that we were\n");
	printf("\t\tnot able to debug completely in time\n");
	printf("\tmessage.cpp - contains the class outline of an object designed\n");
	printf("\t\tto encapsulate message encryption and decryption\n");
	printf("\trsakeypair.cpp - contains classes to handle the storage and retrieval\n");
	printf("\t\tof RSA Keys through ASCII 'armor' and a fixed width format.\n");
	printf("\tprimenumber.cpp - contains an implemention of several Number Theory functions\n");
	printf("\t\tsuch as Euclid's Algorithm, a GCDfinder, prime generator, etc.\n");
	printf("\ttests.cpp - the implementation of our custom testing framework built\n");
	printf("\t\tto run reliable, repeatable tests on the ENTIRE application, on demand.\n");
	printf("\t\tAll the tests are fully modifiable and self-documented.\n");

	return 0;
}

#include <math.h>
#include <algorithm>
#include <string.h>
#include <malloc.h>

#include "crypto-layout.h"
#include "assert.h"

BitIndex& BitIndex::operator=(u8 newValue)
{
	// in this case, we must throw an error if invalid object

	ASSERT( BufferPtr != NULL && WhichBit < 8 );

	u8 flag = 1 << WhichBit;

	if( !newValue ) *BufferPtr &= ~flag;
	else *BufferPtr |= flag;

	return *this;
}

BitIndex::operator bool()
{
	// check for invalid object, fail silently
	if( BufferPtr == NULL )
	{
		return false;
	}

	ASSERT( WhichBit < 8 );

	return ((*BufferPtr & (1<<WhichBit)) != 0) ? true : false;
}

u8 nint::InvalidIndex = 0;
		
nint::nint():
BitCount(0),Bits(NULL),Accessor(NULL,0)
{
}
		
// creates a new object of numBits precision and an initial 32bit
// value (to seperate it from the other constructor below)
nint::nint( u32 numBits, u32 initialValue ):
BitCount(0),Bits(NULL),Accessor(NULL,0)
{
	ASSERT( resizeToNBits(numBits) == numBits );

	from32Bit( initialValue );
}
	
// copies the other variables precision (bit count) and value
nint::nint( const nint& initialValue ):
BitCount(0),Bits(NULL),Accessor(NULL,0)
{
	// resize our buffer to match

	ASSERT( resizeToNBits( initialValue.BitCount ) == initialValue.BitCount );

	// double test

	ASSERT( Bits != NULL );

	// and now copy over all the data to our new buffer

	int ByteLength = (int) ceil(initialValue.BitCount/8.0f);
	memcpy(Bits, initialValue.Bits, ByteLength);

	BitCount = initialValue.BitCount;
}
		
// cleans up memory for this object
nint::~nint()
{
	// sanity check
	ASSERT(Bits != (u8*) 0xDEADC0DE);

	// if we have allocated memory
	if(Bits)
	{
		// free it
		free(Bits);

		// and set our pointer to a unique value, so that 
		// we'll be able to spot a use-after-free error more readily
		Bits = (u8*) 0xDEADC0DE;
	}

	BitCount = 0;
}
		
		
// different methods of setting this objets value
//  - all these methods operate under the same principle:
//    if the data buffer is large enough to fit the new value
//	    DO NOT REALLOCATE - wipe and set new value
//	  if the data buffer is not big enough, wipe, resize to minimum needed value (or explicity set)
//      and set new value
		
void nint::from32Bit( u32 newValue )
{
	if(!Bits || !BitCount)
	{
		ASSERT( resizeToNBits(32) == 32 );
	}
	else if( BitCount < 32 )
	{
		// sanity check, if BitCount is non-zero, Bits should point to valid memory
		ASSERT( Bits != NULL && Bits != (u8*) 0xDEADC0DE );

		// first, wipe whatever's in there now (with a non-zero pattern)
		int ByteLength = ceil(BitCount/8.0f);

		memset(Bits,0x9C,ByteLength);

		// now free the memory

		free(Bits);

		Bits = NULL;
		BitCount = 0;

		// now reallocate to 32 bits

		ASSERT( resizeToNBits(32) == 32 );
	}
	else if( BitCount > 32 )
	{
		// sanity check
		ASSERT( Bits != NULL && Bits != (u8*) 0xDEADC0DE );

		// wipe the memory

		int ByteLength = ceil(BitCount/8.0f);

		memset(Bits,0x9D,ByteLength);
		memset(Bits,0x00,ByteLength);
	}

	// test, test, and retest
	ASSERT( Bits != NULL && BitCount >= 32 );

	// copy over the new value
	memcpy(Bits,&newValue, 4);
}
void nint::fromNBit( nint& newValue )
{
	// here, our precision is set explicity - we merely copy the precision from the passed in value

	// wipe and free our current buffer, if any
	if( Bits )
	{
		int ByteLength = ceil(BitCount/8.0f);
		memset(Bits,0x9E,ByteLength);
		free(Bits);
		BitCount = 0;
		Bits = NULL;
	}


	// resize our buffer to match

	ASSERT( resizeToNBits( newValue.BitCount ) == newValue.BitCount );

	// double test

	ASSERT( Bits != NULL );

	// and now copy over all the data to our new buffer

	int ByteLength = ceil(newValue.BitCount/8.0f);
	memcpy(Bits, newValue.Bits, ByteLength);

	BitCount = newValue.BitCount;
}

void nint::fromRawArray( u8* bits, u32 bitCount, bool littleEndian )
{
	// check our parameters
	if( bits == NULL || bitCount == 0 )
		return;

	// here our precision is set explicitly, so wipe and free our current buffer

	if( Bits )
	{
		int ByteLength = ceil(BitCount/8.0f);
		memset(Bits,0x9E,ByteLength);
		free(Bits);
		BitCount = 0;
		Bits = NULL;
	}

	// resize our current buffer to match

	ASSERT( resizeToNBits( bitCount ) == bitCount );

	// double check

	ASSERT( Bits != NULL );


	// copy over the bit array

	int ByteLength = ceil(bitCount/8.0f);

	if( littleEndian )
		memcpy(Bits,bits,ByteLength);
	else
	{
		// uh oh, we need to reverse the byte order.  Some ASM would do the trick, but we're not 
		// trying to pry every ounce of speed out of this assignment, either.

		for(int i=0; i < ByteLength; i ++ )
		{
			Bits[i] = bits[ByteLength-1-i];
		}
	}


	BitCount = bitCount;
}

void nint::fromBase64( char* base64Str, u32 byteLength )
{
	// parameter check
	if(base64Str == NULL)
		return;

	// base64 encoding is a method of converting a number to ASCII representation in a very
	// convenient and fast manner.  26 lowercase letters + 26 uppercase letters + 10 digits + 2 special characters = 64 unique values
	// each base64 value is translated into 6 bits (2^6 = 64)

	char base64Table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	ASSERT(strlen(base64Table) == 64);

	int dataLength = byteLength;

	u8* scratchPad = (u8*) malloc(dataLength);		// we temporarily store each base64 character as a seperate byte, before we pack them

	for(int i = 0; i < dataLength; i++ )
	{
		int index = -1;

		for( int k = 0; k < 64; k ++ )
		{
			if( base64Str[i] == base64Table[k] )
			{
				index = k;
				k = 64;
			}
		}

		printf( "index: %d char: %c\n", index, (index != -1) ? base64Table[index] : '?' );

		if( index == -1 )
		{
			// invalid base64 character. stop processing right here

			dataLength = i;

			break;
		}
		else
			scratchPad[byteLength-1-i] = index;			// copy over the numeric value
	}

	if( dataLength == 0 )
	{
		// something happened to our data!  oh well, JOB WELL DONE!

		return;
	}

	printf( "DataLength: %d\n", dataLength);


	// wipe and free our old buffer, if any
	if( Bits )
	{
		int ByteLength = ceil(BitCount/8.0f);
		memset(Bits,0x9F,ByteLength);
		free(Bits);
		BitCount = 0;
		Bits = NULL;
	}

	// allocate the new buffer with 6 bits per base64 value
	ASSERT( resizeToNBits(dataLength*6) == dataLength*6 );

	// now, iterate through each value in our scratch pad, and pack them tight
	// 0, 6, 12, 18 <-  4 base64 values per 3 bytes
	// 00000011 11112222 22333333

	int placeHolder = 0,state = 0;

	for( int i = 0; i < dataLength; i ++ )
	{
		switch(state)
		{
		case 0:
			Bits[placeHolder++] = (u8) (((u32)scratchPad[i]&0x3F) & (((u32)scratchPad[i+1]&0x03)<<6));

			state++;
			break;
		case 1:
			Bits[placeHolder++] = (u8) ((((u32)scratchPad[i]&0x3F)>>2) & (((u32)scratchPad[i+1]&0x08)<<4));

			state++;
			break;
		case 2:
			Bits[placeHolder++] = (u8) ((((u32)scratchPad[i]&0x3F)>>4) & (((u32)scratchPad[i+1]&0x3F)<<2));

			state = 0;
			break;
		}
	}
}
		
u32  nint::to32Bit( bool* overflow )
{
	if( !Bits || !BitCount )
		return 0;

	// if this number is bigger than 0xFFFFFFFF (actual value) set the
	// overflow parameter to true

	printf("\nhere\n");

	if(overflow)
	{
		printf("in\n");

		*overflow = false;
	
		u32 ByteLength = ceil(BitCount/8.0f);

		printf("ByteLength: %d\n",ByteLength);

		for( int i = 4; i < ByteLength; i++ )
		{
			printf("%u\n", Bits[i]);
			if( Bits[i] != 0 )
			{
				*overflow = true;

				printf("setting overflow\n");

				i = ByteLength;
			}
		}
	}

	printf("\n\n");

	return *((u32*)Bits);
}

void nint::toRawArray(u8** outputBuffer, u32* outputByteCount )
{
	if( !Bits || !BitCount || !outputBuffer || !outputByteCount )
		return;

	u32 ByteLength 	 = ceil(BitCount/8.0f);

	*outputBuffer 	 = (u8*) malloc(ByteLength);
	*outputByteCount = ByteLength;

	// check out malloc didn't fail

	ASSERT( *outputBuffer != NULL );

	memcpy(*outputBuffer, Bits, ByteLength);
}

void nint::toBase64(char** outputBuffer, u32* outputByteCount )
{
	if( !Bits || !BitCount || !outputBuffer || !outputByteCount )
		return;

	char base64Table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	ASSERT(strlen(base64Table) == 64);

	u32 ByteLength 		= (int) ceil(BitCount/8.0f);
	u32 MaxBase64Length	= (int) ceil(BitCount/6.0f);
	u32 MinBase64Length = (int) floor(BitCount/6.0f);

	*outputBuffer 		= (char*) malloc(MaxBase64Length+1);
	*outputByteCount	= MaxBase64Length+1;

	u8* scratchPad		= Bits;
	u32 state		 	= 0, value = 0;

	ASSERT( scratchPad != NULL );

	for( int i = MinBase64Length; i > 0; i-- )
	{
		switch(state)
		{
		case 0:
			value = scratchPad[0]&0x3F;
			state++;
			break;
		case 1:
			value = (((u32)scratchPad[0]&0xC0)>>6) | (((u32)scratchPad[1]&0x0F)<<2);
			scratchPad ++;
			state++;
			break;
		case 2:
			value = (((u32)scratchPad[0]&0xF0)>>4) | (((u32)scratchPad[1]&0x03)<<4);
			scratchPad ++;
			state++;
			break;
		case 3:
			value = (((u32)scratchPad[0]&0xFC)>>2);
			scratchPad ++;
			state = 0;
			break;
		}

		// base64 values are all smaller than 2^6
		ASSERT( value < 64 );

		(*outputBuffer)[i-1] = base64Table[value];
	}

	if( MaxBase64Length != MinBase64Length )
	{
		// we have an odd precision, we need to extend the last few bits with zeros

		switch(state)
		{
		// can't be in state 0 (that'd fall on a byte boundary)
		case 1:
			value = (((u32)Bits[ByteLength-1] &0xC0) >> 6);
			break;
		case 2:
			value = (((u32)Bits[ByteLength-1] &0xF0) >> 4);
			break;
		default:
			ASSERT( state == 1 || state == 2 );
			break;
		}

		// sanity check
		ASSERT( value < 64 );

		(*outputBuffer)[0] = base64Table[value];
	}

	// null terminated for ease of use

	(*outputBuffer)[MaxBase64Length] = '\0';
}

// returns a reference to the byte at 'index'; throws an error if index is out of bounds
u8& nint::byteAt(u32 index)
{
	if( !Bits || ! BitCount )
		return nint::InvalidIndex;

	if( index >= ((u32) ceil(BitCount/8.0f)))
		return nint::InvalidIndex;

	return Bits[index];
}
			
		
// resizes the number of bits in this number to a new value, witih
// the returned value set to : 
//		+newPrecision, if there were no problems
//		1			 , if there was no change
//	 	0			 , if an error occured (e.g. Out Of Memory, Invalid Parameter, etc)
//		-truncation  , the bit precision that was truncated by this operation

int nint::resizeToNBits( u32 newPrecision )
{
	// 3 potential paths:

	if( newPrecision == BitCount )
		return 1;

	//printf("resizeToNBits: 2nd if\n");

	if( newPrecision > BitCount )
	{
		u32 ByteLength 	= (u32) ceil(newPrecision/8.0f);
		u8* newBuffer 	= (u8*) malloc(ByteLength);

		//printf("resizeToNBits allocated malloc 2nd path\n");

		if( newBuffer == NULL ) return 0;

		//printf("resizeToNBits malloc ok");

		// clear our new buffer to zero

		memset( newBuffer, 0x00, ByteLength );

		//printf("resizeToNBits memset'd ok\n");

		// copy over previous data

		if( Bits )
		{
			printf("resizeToNBits cleared old data\n");

			memcpy(newBuffer, Bits, (u32) ceil(BitCount/8.0f));

			free(Bits);
		}

		// set internal variables

		Bits 		= newBuffer;
		BitCount 	= newPrecision;

		//printf("resizeToNBits everything set, returnning %d\n", BitCount);

		return BitCount;
	}
	else
	{
		u32 ByteLength 	= (u32) ceil(newPrecision/8.0f);
		u8* newBuffer 	= (u8*) malloc(ByteLength);

		if( newBuffer == NULL ) return 0;

		memset( newBuffer, 0x00, ByteLength );

		// if we have data in the buffer, we need to copy it over

		if( Bits )
		{
			free(Bits);

			Bits = NULL;
		}

		// if our new precision isn't on a byte boundary, we have leftover
		// bits from our truncate operation that we need to clear

		int rem = newPrecision % 8;

		if( rem )
		{
			int mask = 0;
			for( int j = 0; j < rem; j++ )
				mask |= (1<<j);

			newBuffer[ByteLength-1] &= mask;
		}

		// set our internal values
		
		int truncate = BitCount - newPrecision;

		Bits 		= newBuffer;
		BitCount 	= newPrecision;

		return -truncate;
	}

	// should never get here

	return 0;
}

// key helper functions that allow comparison of 2 n-precision unsigned integers

// this function will iterate through each bit in every byte starting from the Most Significant Byte
// testing for a XOR condition (we can't actually use xor because the object precisions may be different)
// 	i.e. one bit is true (max) and one is false (min)

nint& nint::nmin( nint& A, nint& B )
{
	u32 AByteLen = (u32) ceil(A.getNBitPrecision()/8.0f);
	u32 BByteLen = (u32) ceil(B.getNBitPrecision()/8.0f);
	u32 maxLen   = std::max(AByteLen, BByteLen);

	for( int i = maxLen; i > 0; i -- )
	{
		for( int bit = 7; bit >= 0; bit -- )
		{
			bool isZero = true;

			if( i <= AByteLen )
			{
				if(A.byteAt(i-1) & (1<<bit))
					isZero = false;
			}

			if( i <= BByteLen )
			{
				if((B.byteAt(i-1) & (1<<bit)) && isZero )
					return A;
			}

			if( !isZero )
				return B;
		}
	}

	// if we make it this far, the two objects are identical

	return A;
}

// this function will iterate through each bit in every byte starting from the Most Significant Byte
// testing for a XOR condition (we can't actually use xor because the object precisions may be different)
// 	i.e. one bit is true (max) and one is false (min)

nint& nint::nmax( nint& A, nint& B )
{
	u32 AByteLen = (u32) ceil(A.getNBitPrecision()/8.0f);
	u32 BByteLen = (u32) ceil(B.getNBitPrecision()/8.0f);
	u32 maxLen   = std::max(AByteLen, BByteLen);

	for( int i = maxLen; i > 0; i -- )
	{
		for( int bit = 7; bit >= 0; bit -- )
		{
			bool isZero = true;

			if( i <= AByteLen )
			{
				if(A.byteAt(i-1) & (1<<bit))
					isZero = false;
			}

			if( i <= BByteLen )
			{
				if((B.byteAt(i-1) & (1<<bit)) && isZero )
					return B;
			}

			if( !isZero )
				return A;
		}
	}

	// if we make it this far, the two objects are identical

	return A;
}

nint& nint::inverse( nint& A, nint& Result )
{
	ASSERT(Result.resizeToNBits(A.getNBitPrecision()) != 0);

	Result = 0;

	if( A.getNBitPrecision() == 0 )
		return Result;

	u32 ByteLength = (u32) ceil(A.getNBitPrecision()/8.0f);
	u8* scratchPtr = &Result.byteAt(0);

	for( int i = 0; i < ByteLength; i++ )
	{
		*scratchPtr = ~A.byteAt(i);

		scratchPtr ++;
	}

	return Result;
}

// performs a bitwise comparison of two objects
bool nint::equal( nint& A, nint& B )
{
	if( !A.getNBitPrecision() || !B.getNBitPrecision() )
	{
		// if they're both null
		if( A.getNBitPrecision() == B.getNBitPrecision() )
			return true;

		// they're different
		return false;
	}

	u32 AByteLen = (u32) ceil(A.getNBitPrecision() / 8.0f);
	u32 BByteLen = (u32) ceil(B.getNBitPrecision() / 8.0f);
	u32 maxLen   = std::max( AByteLen, BByteLen );

	for( int i = 0; i < maxLen; i ++ )
	{
		u8 byte = 0;

		if( i < AByteLen )
			byte = A.byteAt(i);

		if( i < BByteLen )
		{
			if( byte != B.byteAt(i) )
				return false;
		}
		else if( byte )
			return false;
	}

	return true;
}

bool nint::lessThan( nint& A, nint& B )
{
	if( !A.getNBitPrecision() && B.getNBitPrecision() )
		return true;

	if( A.getNBitPrecision() && !B.getNBitPrecision() )
		return false;

	u32 AByteLen = (u32) ceil(A.getNBitPrecision() / 8.0f);
	u32 BByteLen = (u32) ceil(B.getNBitPrecision() / 8.0f);
	u32 maxLen   = std::max( AByteLen, BByteLen );

	for( int i = maxLen-1; i >= 0; i -- )
	{
		u8 byte = 0;

		if( i < AByteLen )
			byte = A.byteAt(i);

		if( i < BByteLen )
		{
			if( byte < B.byteAt(i) )
				return true;
		}
		else if( byte )
			return false;
	}

	// if they are equal, false
	return false;
}

// helper functions that operate on this object and another and
// store the result in this object

nint& nint::mul( nint& A, nint& B, nint& Product )
{
	// resize our product to the sum of the two factors precision

	ASSERT(Product.resizeToNBits(A.getNBitPrecision() + B.getNBitPrecision()) != 0);

	// clear our final variable

	Product = 0;

	// simple multiplication, add A to Product, B times

	for( nint i(32,0); i < B; i = i + 1 )
	{
		Product = Product + A;
	}

	return Product;
}

nint& nint::div( nint& A, nint& B, nint& Quotient )
{
	// if the divisor is bigger than the dividend, return immediately
	if( nint::nmax(A,B) != A )
	{
		Quotient.from32Bit(0);
		return Quotient;
	}

	ASSERT( Quotient.resizeToNBits(A.getNBitPrecision()) != 0);


	nint tracker(A.getNBitPrecision(),0);

	while( nint::nmax(tracker,A) == A )
	{
		tracker = tracker + B;

		if( nint::nmax(tracker,A) != A )
			break;

		Quotient = Quotient + 1;

	}

	return Quotient;
}

u8 nint::fullAdder(u8 A, u8 B, u8 InCarry, u8& OutCarry)
{
	u8 retVal = 0;
	u8 Carry = (InCarry)?1:0;

	for( int i = 0; i < 8; i ++ )
	{
		u8 mask = 1 << i;
		u8 tmp  = 0;

		// first, calculate our output bit (A.Bit XOR B.Bit XOR Carry)
		tmp = ((A&mask) ^ (B&mask)) ^ (Carry<<i);

		// next, calculate our carry bit (!Output.Bit OR (A.Bit AND B.Bit AND Carry))
		// my digital logic teacher would kill me for this...
		Carry = ((A&mask) && (B&mask) && !(Carry)) || (!(A&mask) && (B&mask) && (Carry)) || ((A&mask) && !(B&mask) && (Carry));

		retVal |= tmp;
	}

	OutCarry = Carry;

	return retVal;
}

nint& nint::add(  nint& A,  nint& B, nint& Sum )
{
	ASSERT( A.getNBitPrecision() != 0 );
	ASSERT( B.getNBitPrecision() != 0 );

	// the maximum number of digits from any addition is the maximum digit count + 1
	ASSERT( Sum.resizeToNBits(std::max(A.getNBitPrecision(),B.getNBitPrecision())+1) != 0 );

	Sum = 0;

	// iterate through each byte and perform a full adder addition
	u32 AByteLen = (u32) ceil(A.getNBitPrecision()/8.0f);
	u32 BByteLen = (u32) ceil(B.getNBitPrecision()/8.0f);
	u32 maxLen   = (u32) std::max(AByteLen,BByteLen);

	u8* scratchPtr = &Sum.byteAt(0);
	u8 Carry = 0;

	ASSERT( scratchPtr != NULL );

	for( int i = 0; i < maxLen; i++ )
	{
		u8 AByte   = 0;
		u8 BByte   = 0;

		if( i < AByteLen )
			AByte = A.byteAt(i);

		if( i < BByteLen )
			BByte = B.byteAt(i);

		u8 OutByte = nint::fullAdder(AByte, BByte, Carry, Carry);

		*scratchPtr = OutByte;

		printf( "i: %d O: %u\n", i, *scratchPtr );

		scratchPtr ++;
	}

	return Sum;
}

nint& nint::sub(  nint& A,  nint& B, nint& Difference )
{
	// since we only deal with unsigned values, if B > A, immediately return 0
	if( nint::nmax(B,A) != A )
	{
		Difference.from32Bit(0);

		return Difference;
	}

	ASSERT( A.getNBitPrecision() != 0 );
	ASSERT( B.getNBitPrecision() != 0 );

	nint TwosComp(1);

	// take the 2s complement of the B parameter
	nint::inverse(B, TwosComp);

	// now we can just add them together
	nint::add(A,TwosComp, Difference);

	return Difference;
}

nint& nint::modulo( nint& A, nint& B, nint& Result )
{
	// if the divisor is bigger than the dividend, return immediately
	if( nint::nmax(B,A) != A )
	{
		Result = (nint::equal(A,B))?nint(0):B;

		return Result;
	}

	ASSERT( Result.resizeToNBits(A.getNBitPrecision()) != 0);
	
	nint Quotient(A.getNBitPrecision(),0);
	nint tracker(A.getNBitPrecision(),0);

	while( nint::nmax(tracker,A) == A )
	{
		tracker = tracker + B;

		if( nint::nmax(tracker,A) != A )
			break;

		Quotient = Quotient + 1;
	}

	tracker = tracker - B;

	Result = A - tracker;

	return Result;
}

nint& nint::power(  nint& A,  nint& Exponent,  nint& Modulo, nint& Result )
{
	/*	Successive Square Algorithm
	 *
	 * A useful computational tool to calculate very large exponents in a finite field (such as with congruency and modulo operators)
	 *	
	 *			Eq.   a ^ X (mod M) is congruent to ____________
	 *
	 *	Step 1. Break 'X' into its unique factors as powers of 2 (easily accomplished using bitwise operators)
	 *	Step 2. Build the table of values we'll reference later.  Each row is the square of the previous one, mod M. <- IMPORTANT!
	 *	Step 3. Foreach power of 2 factor in 'X', multiply the corresponding table rows (with intermediate mod M if necessary)
	 *	Step 4. Profit.
	 *
	*/

	 // Result is congruent to A ^ B (mod Modulo)

	 // Step 1: (here we just need to know how many factors there are)

	 u32 ByteLength 	= (u32) ceil(Exponent.getNBitPrecision() / 8.0f);
	 u32 maxFactor	 	= 0;

	 // determine the maximum power of 2 that is a factor of the exponent

	 for( int i = 0; i < ByteLength; i ++ )
	 	for( int j = 0; j < 8; j ++ )
	 		if( Exponent.byteAt(i) & (1<<j) )
	 			maxFactor = std::max((u32) maxFactor,(u32) (i*8)+j);

	 // Step 2:

	 nint *SquareTable = (nint*) malloc( sizeof(nint) * maxFactor );
	 nint  value(A.getNBitPrecision(), 1);

	 for( int i = 0; i < ByteLength; i ++ )
	 	for( int j = 0; j < 8; j ++ )
	 	{
	 		nint tmp;

	 		nint::mul(value,value,tmp);
	 		nint::modulo(tmp,Modulo,value);

	 		SquareTable[(i*8)+j] = value;
	 	}
}

nint nint::operator+( nint& B )
{
	nint tmp = 0;

	printf( "operator+\n" );

	nint::add(*this, B, tmp); 

	fromNBit(tmp);

	return *this; 
}

nint nint::operator-( nint& B )
{
	nint tmp = 0; 

	nint::sub(*this, B, tmp); 

	fromNBit(tmp);

	return *this; 
}

nint nint::operator/( nint& B )
{
	nint tmp = 0; 

	nint::div(*this, B, tmp);  

	fromNBit(tmp);

	return *this; 
}

nint nint::operator*(  nint& B )
{
	nint tmp = 0; 

	nint::mul(*this, B, tmp);  

	fromNBit(tmp);

	return *this; 
}

nint nint::operator%(  nint& B )
{
	nint tmp = 0; 

	nint::modulo(*this, B, tmp);  

	fromNBit(tmp);

	return *this; 
}

// overload the [] operator both as a quick way to retrieve a bit's value
// but a second time so we can easily set it.
	
BitIndex& nint::operator[]( int index )
{
	if( !Bits || !BitCount )
	{
		Accessor.setBuffer(&nint::InvalidIndex);
		Accessor.setBit(0);
	}

	ASSERT( index > 0 && index < BitCount );

	Accessor.setBuffer(&Bits[(int) floor(BitCount/8.0f)]);
	Accessor.setBit(index % 8);

	return Accessor;
}

u8 nint::operator[]( int index ) const
{
	ASSERT( Bits && BitCount );

	ASSERT( index > 0 && index < BitCount );

	u32 ByteOff = (u32) floor(index/8.0f);

	return ((Bits[ByteOff] & (1<<(index - (ByteOff*8)))) != 0) ? 1 : 0;
}
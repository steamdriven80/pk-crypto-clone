#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <malloc.h>
#include <math.h>
#include <string.h>

#include <stdio.h>
#include <inttypes.h>

#include "assert.h"
#include "crypto-layout.h"

#include "tests.h"	

bool testGroup( u32 group )
{
	switch( group )
	{
	case TestGroup_nint:

		//return Tests::testNINT();

		break;

	case TestGroup_PrimeNumber:

		return Tests::testPRIME();

	case TestGroup_Base64:

		return Tests::testBASE64();

	default:

		break;
	}

	return true;
}
/*
bool Tests::testNINT()
{
	TestStart("fullAdder",true)
	{
		u8 A = 90, B = 100, Carry = 0, R;
		R = nint::fullAdder( A, B, Carry, Carry );

		TestAssert( R == 190 );

		A = 0; B = 0; Carry = 1;
		R = nint::fullAdder( A, B, Carry, Carry );

		TestAssert( R == 1 );

		A = 255; B = 2; Carry = 0;
		R = nint::fullAdder( A, B, Carry, Carry );

		TestAssert( R == 1 && Carry == 1 );
	}

	// test all the constructors

	TestStart("constructors", true)
	{
		nint test1;

		TestAssert( test1.BitCount == 0 );

		TestAssert( test1.Bits == NULL );


		nint test2(503);

		TestAssert( test2.BitCount == 32 );

		TestAssert( test2.Bits != NULL );

		TestAssert( *((u32*)test2.Bits) == 503 );


		nint test3(test2);

		TestAssert( test3.BitCount == test2.BitCount );

		TestAssert( test3.Bits != NULL );

		TestAssert( *((u32*)test3.Bits) == *((u32*)test2.Bits) );


		nint test4(64, 999);

		TestAssert( test4.BitCount == 64 );

		TestAssert( test4.Bits != NULL );

		TestAssert( test4 == (nint) 999 );		// tests the int->nint constructor and comparison operator
	}

	TestStart("regular precision", true)
	{
		nint test1, test2(32005);

		test1 = 32005;
		test2 = 32111;

		TestAssert( test1 != test2 );

		TestAssert( test1 == 32005 );

		TestAssert( test2 == 32111 );

		test1 = test1 + 106;

		TestAssert( test1 == 32111 );

		TestAssert( test2 == test1 );
	}

	TestStart("large precision", true)
	{
		nint 	test1(64,0);

		char*   buffer;
		u32		bufferLen;

		TestAssert( test1.BitCount == 64 );

		TestAssert( test1.to32Bit() == 0 );


		test1 = 600;

		TestAssert( test1.BitCount == 64 );

		TestAssert( test1.to32Bit() == 600 );


		nint 	test2(64,0);

		TestAssert( test2.getNBitPrecision() == 64 );

		TestAssert( (test1 - 599) != test2 );

		TestAssert( (test1 - 600) == test2 );

		nint 	test3(128,500);

		TestAssert( test3.BitCount == 128 );

		TestAssert( test3.to32Bit() == 500 );


		test3 = test3 * 3;

		TestAssert( test3.BitCount == 128 );

		TestAssert( test3.to32Bit() == 1500 );


		nint 	test4(1024,65535);

		TestAssert( test4.BitCount == 1024 );

		TestAssert( test4.to32Bit() == 65535 );

		test2 = ~((u32)0);

		nint one(64,1);

		TestAssert( test2.to32Bit() == 4294967295 );

		nint::add(test2,one,test4);


		bool overflow = false;

		printf("Before: %d\n", overflow );

		u32 R = test4.to32Bit(&overflow);

		printf("After: %d\n", overflow );

		TestAssert( R == 0 );		// with overflow set
		TestAssert( overflow );

		//TestAssert( test4.to32Bit(overflow) == 4294967294 );
		//TestAssert( overflow == false );
	}
}*/

bool Tests::testPRIME()
{
	TestStart("generatePrimes",true)
	{
		PrimeNumber prime;

		prime.generatePrime(0);

		InfoStatusArgs(Prime[0] Value: %lld, prime.Value);

		TestAssert(prime.checkPrime(prime.Value));

		prime.generatePrime(100);

		InfoStatusArgs(Prime[1] Value: %lld, prime.Value);

		TestAssert(prime.checkPrime(prime.Value));

		prime.generatePrime(1000000);

		InfoStatusArgs(Prime[2] Value: %lld, prime.Value);

		TestAssert(prime.checkPrime(prime.Value));
	}

	TestStart("randomizePrime",true)
	{
		PrimeNumber prime, prime2;
		
		srand( time(NULL) );

		u64 value = (sqrt((rand()/((float)RAND_MAX))*(~((u32)0) - 1000)));

		InfoStatusArgs(Randomized Value[0]: %lld, value);

		prime.generatePrime(value);
		value = PrimeNumber::sSquare(value,8,10000);

		InfoStatusArgs(Randomized Value Raised ^8 (mod 10000): %lld, value);


		
		value = (sqrt((rand()/((float)RAND_MAX))*(~((u32)0) - 1000)));

		InfoStatusArgs(Randomized Value[1]: %lld, value);

		prime.generatePrime(value);
		value = PrimeNumber::sSquare(value,8,10000);

		InfoStatusArgs(Randomized Value Raised ^8 (mod 10000): %lld, value);
	}

	TestStart("kRoot",true)
	{
		PrimeNumber p1, p2, p3;

		srand(time(NULL));

		u64 value = (rand()/((float)RAND_MAX))*5000.0+5000;
		p1.generatePrime(value);
		value = (rand()/((float)RAND_MAX))*5000.0+5000;
		p2.generatePrime(value);

		do
		{
			value = (rand()/((float)RAND_MAX))*30+20;
			p3.generatePrime(value);
		} while(PrimeNumber::gcd(p3.Value, (p1.Value-1) * (p2.Value-1)) != 1);

		InfoStatusArgs([0] Prime Value: %lld, p1.Value);
		InfoStatusArgs([1] Prime Value: %lld, p2.Value);
		InfoStatusArgs([2] Prime Value: %lld, p3.Value);

		s64 number = 32;

		TestAssert( PrimeNumber::gcd(p3.Value, (p1.Value-1) * (p2.Value-1)) == 1 );

		number = PrimeNumber::sSquare(number, p3.Value, p1.Value * p2.Value);

		InfoStatusArgs(Encrypted Data: %lld, number);

		number = PrimeNumber::kRoot( p3.Value, number, p1.Value, p2.Value);

		InfoStatusArgs(Decrypted Data: %lld, number);

		TestAssert( number == 32 );
	}
}

bool Tests::testBASE64()
{
	TestStart("Encode/Decode a Value",true)
	{
		s64 someValue = 2342134214542365;

		char* buffer = NULL;
		u32   length = 0;

		TestAssert( base64Enc((u8*) &someValue, sizeof(someValue), &buffer) != NULL );

		InfoStatusArgs( Encoded Base 64 Value : %s, buffer );

		length = strlen(buffer);

		s64 newValue = 0;
		u8* newBuffer = NULL;
		u32 newLength = 0;

		TestAssert( base64Dec(buffer, length, &newBuffer, &newLength) != NULL );

		TestAssert( newLength == sizeof(someValue) );

		newValue = *((s64*) newBuffer);

		TestAssert( newValue == someValue );
	}

	TestStart("Encode/Decode a long buffer",true)
	{
		u8 buffer[1024], *outBuffer;
		u32 outLength;

		srand(time(NULL));

		for( int i = 0; i < 1024; i ++ )
		{
			buffer[i] = rand() & 0xFF;
		}


		char* encodedString;

		TestAssert( base64Enc(buffer,1024,&encodedString) != NULL );

		InfoStatus( Encoded Base 64 Long Buffer : );

		for( int i = 0; i < 16; i++ )
			printf( "\t%-64.64s\n", &encodedString[i*64] );

		TestAssert( base64Dec(encodedString,strlen(encodedString),&outBuffer, &outLength) != NULL );

		TestAssert( outLength == 1024 );

		bool Equal = true;

		for( int i = 0; i < 1024; i ++ )
		{
			if( outBuffer[i] != buffer[i] )
			{
				Equal = false;
				break;
			}
		}

		if( Equal )
			PassStatus( Decoded Data Matches Original Data );
		else
			FailStatus( Decoded Data Doesnt Match Original Data );
	}

	//#undef TestAssert
	//#define TestAssert(expr) expr

	TestStart("RSAKeyPair",true)
	{
		RSAKeyPair keyPair;

		RSAKey RestoredPublic, RestoredPrivate;

		TestAssert( keyPair.generateKey() == true );

		RSAKey Public, Private;



		Public = keyPair.getPublicKey();
		Private = keyPair.getPrivateKey();

		InfoStatusArgs( Public Key Generated.  u64 Value %llu, Public.getPrime().getValue());
		InfoStatusArgs( Private Key Generated.  u64 Value %llu, Private.getPrime().getValue());



		char* buffer;

		TestAssert( Public.prettyPrint(&buffer) != NULL );

		InfoStatusArgs( Public Key Pretty Print: \n%s, buffer );

		TestAssert( RestoredPublic.loadFromString( buffer ) == true );

		delete buffer;



		TestAssert( Private.prettyPrint(&buffer) != NULL );

		InfoStatusArgs( Private Key Pretty Print: \n%s, buffer );

		TestAssert( RestoredPrivate.loadFromString( buffer ) == true );

		InfoStatusArgs( RestoredPublic Key Loaded.  u64 Value %llu, RestoredPublic.getPrime().getValue());
		InfoStatusArgs( RestoredPrivate Key Loaded.  u64 Value %llu, RestoredPrivate.getPrime().getValue());

		TestAssert( RestoredPublic.getPrime().getValue() == Public.getPrime().getValue() );
		TestAssert( RestoredPublic.getExponent() == Public.getExponent() );
		TestAssert( RestoredPrivate.getPrime().getValue() == Private.getPrime().getValue() );
		TestAssert( RestoredPrivate.getExponent() == Private.getExponent() );
	}
}

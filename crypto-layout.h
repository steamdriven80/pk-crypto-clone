#ifndef _CRYPTO_LAYOUT_H_
#define _CRYPTO_LAYOUT_H_


/*********************************************************************
*
*	crypto-layout.h
* 
* 	first draft: 31 Oct 2014  Chris Pergrossi
*
* 	a description of the various parts to the RSA public/private key
* 	encryption software written as the semester project for COP3503
*
**********************************************************************/

// A meaningless value intended to allow us to track when we make file format changes
// in case something breaks

#define BuildVersion 0x1010


// These values govern the minimum and maximum seeds for our key generation algorithm

#define PRIME_MIN 	10000
#define PRIME_RANGE	10000.0

#define K_MIN	20
#define K_RANGE 30.0


// I prefer these explicit types so I typedef them here

typedef unsigned int u32;
typedef   signed int s32;
typedef unsigned short u16;
typedef   signed short s16;
typedef unsigned char u8;
typedef   signed char s8;

typedef unsigned long long u64;
typedef   signed long long s64;

/***********************************************************************
 * base64Enc() and base64Dec()
 *
 *	These functions are vital for working with these huge numbers.  Printing
 *  them to screen, reviewing them, saving/loading them in ASCII format - all
 *  immeasurably easier using base64
 ************************************************************************/

char* 	base64Enc( u8* input, u32 inputLen, char** outputBuffer );
u8* 	base64Dec( char* input, u32 inputLen, u8** outputBuffer, u32* outputLen );

/***************************************************************************
 *
 * UNFORTUNATELY THIS OBJECT DID NOT TURN OUT AS RELIABLE AS NEEDED IN TIME
 * better to go with a lesser solution than one that breaks unpredictably
 *
 ***************************************************************************/

// We'll need an N-bit number object - unfortunately, 32 or even 64 bits
// is not nearly enough address space to provide safety against bruteforce
/*
class BitIndex
{
protected:
	u8*		BufferPtr;
	u8		WhichBit;

	friend class Tests;

public:

	BitIndex(u8* ByteBoundaryPtr, u8 BitIndex ):
	BufferPtr(ByteBoundaryPtr),WhichBit(BitIndex)
	{
	}

	void setBuffer(u8* buffer) { BufferPtr = buffer; }
	void setBit(u8 bit) { WhichBit = bit; }

	BitIndex& operator=(u8 newValue);

	operator bool();

	bool isValid() { return (BufferPtr != NULL && WhichBit < 8); }
};

class nint
{
	protected:
	
		// the number of bits in this object	
		u32 	BitCount;
		
		// a dynamically allocated buffer that holds this integer
		// we store each byte in little endian order like this:
		//
		// LOWER MEMORY ------------->-------------->  HIGHER MEMORY
		// LSB          ------------->-------------->  MSB
		//
		
		u8		*Bits;

		// our local index accessor
		BitIndex Accessor;

		friend class Tests;

		// u8 invalid object placeholder
		static u8		InvalidIndex;

		static u8		fullAdder(u8 A, u8 B, u8 InCarry, u8& OutCarry);
		
	public:
	
		nint();
		
		// creates a new object of numBits precision and an initial 32bit
		// value (to seperate it from the other constructor below)
		nint( u32 numBits, u32 initialValue );

		// allows conversion of a u32 to an nint implicitly

		nint( u32 initialValue ): Bits(NULL),BitCount(0),Accessor(NULL,0) { from32Bit(initialValue); }

		// copies the other variables precision (bit count) and value
		nint( const nint& initialValue );
		
		// cleans up memory for this object
		~nint();
		
		
		// different methods of setting this objets value
		//  - all these methods operate under the same principle:
		//    if the data buffer is large enough to fit the new value
		//	    DO NOT REALLOCATE - wipe and set new value
		//	  if the data buffer is not big enough, wipe, resize to minimum needed value (or explicity set)
		//      and set new value
		
		void from32Bit( u32 newValue );
		void fromNBit(  nint& newValue );
		void fromRawArray(  u8* bits, u32 bitCount, bool littleEndian = true );
		void fromBase64(  char* base64, u32 byteLength );

		u32  to32Bit( bool* overflow = NULL );
		void toRawArray(u8** outputBuffer, u32* outputByteCount );
		void toBase64(char** outputBuffer, u32* outputByteCount );

		// returns the invalid index object ( its address can be used to test for no data) (buts its value is not guaranteed)
		static u8		InvalidAddress;
		
		// resizes the number of bits in this number to a new value, witih
		// the returned value set to : 
		//		+newPrecision, if there were no problems
		//		1			 , if there was no change
		//	 	0			 , if an error occured (e.g. Out Of Memory, Invalid Parameter, etc)
		//		-truncation  , the bit precision that was truncated by this operation
		
		int resizeToNBits( u32 newPrecision );
		u32 inline getNBitPrecision() { return BitCount; }

		// returns a reference to the byte at 'index'; throws an error if index is out of bounds
		u8& byteAt(u32 index);

		// key helper functions that allow comparison of 2 n-precision unsigned integers

		static nint& nmin(  nint& A,  nint& B );
		static nint& nmax(  nint& A,  nint& B );
	
		// performs a bitwise NOT operation on A and stores the result in Result

		static nint& inverse(  nint& A, nint& Result );

		// performs a bitwise comparison of two objects
		static bool equal(  nint& A,  nint& B );

		// helper functions that operate on this object and another and
		// store the result in this object
		
		static nint& mul(  nint& A,  nint& B, nint& Product );		// A = A * B
		static nint& div(  nint& A,  nint& B, nint& Quotient );		// A = A / B	(dividend only)	
		static nint& add(  nint& A,  nint& B, nint& Sum );			// A = A + B
		static nint& sub(  nint& A,  nint& B, nint& Difference );		// A = A - B

		static bool lessThan( nint& A, nint& B );
		
		static nint& modulo(  nint& A,  nint& B, nint& Remainder );	// A = A % B 	(remainder only)
		static nint& power(  nint& A,  nint& B,  nint& Modulo, nint& Result );		// A = (A ^ B) (mod Modulo)
		
		nint operator=( nint B ) { fromNBit( B ); return *this; }
		nint operator=( u32 B ) { from32Bit( B ); return *this; }
		
		nint addTo( nint B ) { nint tmp; nint::add( *this, B, tmp ); return tmp; }

		nint operator+( nint& B );
		nint operator-( nint& B );
		nint operator/( nint& B );
		nint operator*( nint& B );
		
		nint operator%( nint& B );

		bool operator<( nint& B ) { return nint::lessThan(*this,B); }
		bool operator>( nint& B ) { return nint::lessThan(B,*this); )

		//operator u32() { return to32Bit(); }

		bool operator ==( nint& B ) { return nint::equal( *this, B ); }
	
		// overload the [] operator both as a quick way to retrieve a bit's value
		// but a second time so we can easily set it.
	
		BitIndex& operator[]( int index );
		u8 operator[]( int index) const;
};*/


/***********************************************************************************************
 * 
 * 		PrimeNumber
 * 
 * 		an object encompassing all operations and data associated with a single prime number.
 * 		using our n-bit integer class, we can use this same object for 4 digit prime numbers,
 *  	or 400.
 * 
 * ********************************************************************************************/
 
 
class PrimeNumber
{
	protected:
		
		// the primes value
		
		u64 		Value;



		friend class Tests;

	public:

		// finds the greatest common divisor of two numbers
		static u64 gcd(u64 a, u64 b);

		// extended euclidean algorithm (recursive)
		// result is a 3 element s64 array
		static void xeuclid(s64 a, s64 b, s64 *result );
		
	
	
		PrimeNumber();
		
		~PrimeNumber();
		
		
		// attempts to generate a prime of minDigits precision using brute force
		// DON'T try this for large primes use the methods below
		
		u64 generatePrime( u64 minimum );

		bool isMersennePrime( u64 prime );
				
		bool checkPrime(u64 prime);

		u64 getValue() const { return Value; }

		void setValue(u64 value) { Value = value; }


		// successively square this number to large exponents, but in a finite field (0<= sSquare(exp,mod) < mod)
		static u64 sSquare( u64 base, u64 exponent, u64 modulo );				
		
		// calculates the Kth Root of the equation X ^ k = b (mod pq) and assigns the result
		// to this object.
		
		static u64 kRoot( u64 Exponent, u64 KPower, u64 p, u64 q );
};
 
/*******************************************************************************************
 *
 *		RSAKey
 *
 *		this object encapsulates a single RSA asymmetric key - either public or private. Also
 *		contains some useful methods for manipulating a key.
 *
 ********************************************************************************************/

class RSAKey
{
	protected:

		friend class Tests;

		// the exponent of the key
		u64 		Exponent;

		// the prime number that makes up this side of the key
		PrimeNumber Prime;

		// this makes our container class able to access our properties to set them upon creation
		friend class RSAKeyPair;

	public:

		RSAKey(): Exponent(0) {}

		~RSAKey() {}


		// returns the exponent as an unsigned long long
		u64 getExponent() { return Exponent; }

		// returns the prime number that makes up this part of the key
		PrimeNumber getPrime() { return Prime; }


		// prints this key (the exponent and prime number) in a standard format, base64 encoded and
		// with a fixed width
		//
		//  @retvalue: *buffer for ease of use
		char*		prettyPrint( char** buffer );

		// loads this object from a prettyPrinted RSAKey null-terminated string (as printed by RSAKey::prettyPrint())
		//
		//  @retvalue: true on success, false on failure
		bool		loadFromString( char* data );
};


/*******************************************************************************************
 * 
 * 		RSAKeyPair
 * 
 * 		this object encapsulates an entire public and private key pair
 * 			- generating a new key
 * 			- destroying / invalidating an old one
 *			- exporting key in base64 format
 * 			- importing a key given in base64 format
 * 			- formatting, seeding, and other administrative work with a key pair
 * 
 * *****************************************************************************************/
 
 class RSAKeyPair
 {
	protected:
	
		// the public key
		RSAKey Public;

		// the private key
		RSAKey Private;


		friend class Tests;
	
	public:
	
		RSAKeyPair();
		
		~RSAKeyPair();
		
		
		// if I'm remembering right, the equation is something like this:
		//
		//
		//	Encrypting End:
		//
		//		( Data ) ^ (P k = X (mod pq)
		//
		//	Decrypting End
		//
		//		( X ) ^ (1/k) = Data (mod pq)
		//
		
		
		// generates a new key pair 
		// the return value is Precision if success, or a series of negative error messages.
		
		bool generateKey();

		
		// returns the public key data
		RSAKey getPublicKey() { return Public; }

		// WARNING
		//
		// this returns the private key, this should be used carefully (ie. don't send over unencrypted network)

		RSAKey getPrivateKey() { return Private; }
 };
 
 /*******************************************************************************************
 * 
 * 		Message
 * 
 * 		this object encapsulates a entire message.  The typical order of function calls would
 *		be as follows:
 *			1.  Grab Message From User 			(stdin?)
 *			2. 	Encode the message 				encodeMsg()
 *				(this removes arbitrary characters, and compacts the data into a smaller field.)
 *			3.	Encrypt the message 			encryptMsg() 
 *	[optional]	Sign the message to prove authenticity
 *			4. 	Communicate the encrypted message
 *	[optional]  Verify message signature with sender public key
 *			5.  Receiver Decrypts the message 	decryptMsg()
 *			6.	Receiver Decodes the message 	decodeMsg()
 *			
 * 
 * *****************************************************************************************/

 class Message
 {
 	protected:

 		// the internal data buffer (might be encrypted or decrypted)
 		u8*			Data;

 		// length of buffer
 		u32			Length;

 		friend class Tests;

 	public:

 		Message();

 		~Message();


 		// Encrypts an encoded message using a recipients public key
 		//
		//	 @retvalue:	length of output buffer

 		u32 encryptMsg( char* encodedMsg, char** outputBuffer, RSAKey PublicKey );

 		// Signs an encrypted message with a private key using the length of the encryptedMsg and a randomized salt value
 		//
 		//	 @retvalue: true on success, false if there was an error (most likely the key is invalid)

 		bool signMsg( u8* encryptedMsg, u32 byteLength, RSAKey SignersPrivateKey );

 		// Verifies the signature (if any) on an encrypted message using the [expected] senders public key.
 		// If there is no signature at all, DefaultPass defines the behavior.  If there is a signature but it does
 		// not match, always return false.
 		//
 		// 	 @retvalue: true if signature matches (or NO signature with DefaultPass==true) or false otherwise

 		bool signVerify( u8* encryptedMsg, u32 byteLength, RSAKey SignersPublicKey, bool DefaultPass = false );

 		// Decrypts an encoded message using a private key
 		//
		//	 @retvalue:	length of output buffer

 		u32 decryptMsg( char* encodedMsg, char** outputBuffer, RSAKey PrivateKey );

 		// Prints the contents of the internal buffer using base64 encoding and a fixed width/format (uses newLine for newlines)

 		char* prettyPrint( char** buffer, char newLine = '\n' );
 };


#endif